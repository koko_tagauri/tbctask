﻿using System.Threading.Tasks;
using TbcTask.Domain.CityManagement;
using TbcTask.Domain.Interfaces;
using TbcTask.Domain.UserManagement;
using TbcTask.Infrastructure.DB;
using TbcTask.Infrastructure.Repositories;

namespace TbcTask.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TbcTaskDbContext _dbContext;

        public UnitOfWork(TbcTaskDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IUserRepository UserRepository => new UserRepository(_dbContext);

        public IUserConnectionRepository UserConnectionRepository => new UserConnectionRepository(_dbContext);

        public int Commit() => _dbContext.SaveChanges();

        public async Task<int> CommitAsync() => await _dbContext.SaveChangesAsync();
    }
}
