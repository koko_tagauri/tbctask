﻿using TbcTask.Domain.UserManagement;
using TbcTask.Domain.UserManagement.Models;
using TbcTask.Infrastructure.DB;

namespace TbcTask.Infrastructure.Repositories
{
    public class UserRepository : Repository<User, int>, IUserRepository
    {
        public UserRepository(TbcTaskDbContext context) : base(context)
        {
        }
    }
}
