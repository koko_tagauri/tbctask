﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TbcTask.Domain.UserManagement;
using TbcTask.Domain.UserManagement.Models;
using TbcTask.Infrastructure.DB;

namespace TbcTask.Infrastructure.Repositories
{
    public class UserConnectionRepository : Repository<UserConnection, int>, IUserConnectionRepository
    {
        public UserConnectionRepository(TbcTaskDbContext context) : base(context)
        {
        }

        public async Task<UserConnection> GetByConnectionDetailsAsync(int userId, int connectedUserId)
        {
            var userConnection = await Table.FirstOrDefaultAsync(x => x.UserId == userId && x.ConnectedUserId == connectedUserId);

            return userConnection;
        }

        public IEnumerable<UserConnection> GetUserConnections(int userId)
        {
            var connections = TableNoTracking.Where(x => x.UserId == userId);

            return connections;
        }

        public void RemoveConnections(int userId)
        {
            var connections = Table.Where(x => x.UserId == userId || x.ConnectedUserId == userId);

            Delete(connections);
        }
    }
}
