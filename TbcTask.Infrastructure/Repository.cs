﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TbcTask.Domain.Interfaces;
using TbcTask.Infrastructure.DB;
using TbcTask.Shared;

namespace TbcTask.Infrastructure
{
    public class Repository<TEntity, TKey> : IRepository<TEntity, TKey> where TEntity : BaseEntity<TKey>
    {
        private readonly TbcTaskDbContext _context;

        private DbSet<TEntity> _entities;

        public Repository(TbcTaskDbContext context)
        {
            _context = context;
        }

        public virtual async Task<TEntity> GetByIdAsync(TKey id, bool noTracking = false)
        {
            if (noTracking)
                return await TableNoTracking.FirstOrDefaultAsync(m => m.Id.Equals(id));
            
            return await Entities.FindAsync(id);
        }

        public virtual async Task InsertAsync(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            await Entities.AddAsync(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            _context.Remove(entity);
        }

        public virtual void Delete(IEnumerable<TEntity> entities)
        {
            if (entities == null)
                throw new ArgumentNullException(nameof(entities));

            _context.RemoveRange(entities);
        }

        public virtual async Task<bool> ExistsAsync(TKey id)
        {
            return await Entities.AnyAsync(x => x.Id.Equals(id));
        }

        public async Task<int> CountAsync()
        {
            return await Table.CountAsync();
        }

        public virtual IQueryable<TEntity> Table => Entities;

        public virtual IQueryable<TEntity> TableNoTracking => Entities.AsNoTracking();

        protected virtual DbSet<TEntity> Entities
        {
            get { return _entities ??= _context.Set<TEntity>(); }
        }
    }
}
