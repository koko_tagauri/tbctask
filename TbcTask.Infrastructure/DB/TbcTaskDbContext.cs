﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TbcTask.Domain.CityManagement;
using TbcTask.Domain.CityManagement.Models;
using TbcTask.Domain.UserManagement;
using TbcTask.Domain.UserManagement.Models;
using TbcTask.Shared.Contracts;

namespace TbcTask.Infrastructure.DB
{
    public class TbcTaskDbContext : DbContext
    {
        public TbcTaskDbContext(DbContextOptions<TbcTaskDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(TbcTaskDbContext).Assembly);
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            HandleIDateTrackedEntities();
            return await base.SaveChangesAsync(cancellationToken);
        }

        public override int SaveChanges()
        {
            HandleIDateTrackedEntities();
            return base.SaveChanges();
        }

        private void HandleIDateTrackedEntities()
        {
            var currentDate = DateTimeOffset.Now;
            foreach (var item in ChangeTracker.Entries<ICreateDateTrackedEntity>().Where(entity => entity.State == EntityState.Added))
            {
                item.Entity.CreateDate = currentDate;
            }

            foreach (var item in ChangeTracker.Entries<IChangeDateTrackedEntity>().Where(entity =>
                entity.State == EntityState.Added || entity.State == EntityState.Modified))
            {
                item.Entity.LastChangeDate = currentDate;
            }
        }
    }
}
