﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TbcTask.Domain.UserManagement;
using TbcTask.Domain.UserManagement.Models;

namespace TbcTask.Infrastructure.DB.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(nameof(User));
            builder.HasKey(m => m.Id);

            builder.HasOne(x => x.City)
                .WithMany(x => x.Users)
                .HasForeignKey(x => x.CityId);

            builder.HasMany(x => x.UserPhones)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId);
        }
    }
}
