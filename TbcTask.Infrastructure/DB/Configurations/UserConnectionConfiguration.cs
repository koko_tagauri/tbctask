﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TbcTask.Domain.UserManagement;
using TbcTask.Domain.UserManagement.Models;

namespace TbcTask.Infrastructure.DB.Configurations
{
    public class UserConnectionConfiguration : IEntityTypeConfiguration<UserConnection>
    {
        public void Configure(EntityTypeBuilder<UserConnection> builder)
        {
            builder.ToTable(nameof(UserConnection));
            builder.HasKey(m => m.Id);
        }
    }
}
