﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TbcTask.Domain.UserManagement;
using TbcTask.Domain.UserManagement.Models;

namespace TbcTask.Infrastructure.DB.Configurations
{
    public class UserPhoneConfiguration : IEntityTypeConfiguration<UserPhone>
    {
        public void Configure(EntityTypeBuilder<UserPhone> builder)
        {
            builder.ToTable(nameof(UserPhone));
            builder.HasKey(m => m.Id);
        }
    }
}
