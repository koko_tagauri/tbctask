﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using TbcTask.Domain.CityManagement.Models;

namespace TbcTask.Infrastructure.DB
{
    public static class DbInitializer
    {
        private static TbcTaskDbContext _context;

        public static void Initialize(IServiceProvider serviceProvider)
        {
            _context = serviceProvider.GetService<TbcTaskDbContext>();

            SeedCities();

            _context.SaveChanges();
        }

        private static void SeedCities()
        {
            if (!_context.Set<City>().Any())
            {
                _context.AddRange(new List<City>
                {
                    new City("Tbilisi"),
                    new City("Telavi"),
                    new City("Kutaisi")
                });
            }
        }
    }
}