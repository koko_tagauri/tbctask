﻿using System.Threading.Tasks;
using TbcTask.Domain.CityManagement;
using TbcTask.Domain.UserManagement;

namespace TbcTask.Domain.Interfaces
{
    public interface IUnitOfWork
    {
        IUserRepository UserRepository { get; }

        IUserConnectionRepository UserConnectionRepository { get; }

        int Commit();

        Task<int> CommitAsync();
    }
}
