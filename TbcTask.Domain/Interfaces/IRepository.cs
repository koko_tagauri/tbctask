﻿using System.Linq;
using System.Threading.Tasks;
using TbcTask.Shared;

namespace TbcTask.Domain.Interfaces
{
    public interface IRepository<TEntity, in TKey> where TEntity : BaseEntity<TKey>
    {
        Task<TEntity> GetByIdAsync(TKey id, bool noTracking = false);

        Task InsertAsync(TEntity entity);

        void Delete(TEntity entity);

        Task<bool> ExistsAsync(TKey id);

        Task<int> CountAsync();

        IQueryable<TEntity> Table { get; }

        IQueryable<TEntity> TableNoTracking { get; }
    }
}
