﻿using System.Collections.Generic;
using TbcTask.Domain.UserManagement.Models;
using TbcTask.Shared;

namespace TbcTask.Domain.CityManagement.Models
{
    public class City : BaseEntity<int>
    {
        public City()
        {
        }

        public City(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }

        public virtual ICollection<User> Users { get; private set; }
    }
}
