﻿using TbcTask.Domain.UserManagement.Enums;
using TbcTask.Shared;

namespace TbcTask.Domain.UserManagement.Models
{
    public class UserConnection : BaseEntity<int>
    {
        public UserConnection()
        {
        }

        public UserConnection(int userId, int connectedUserId, UserConnectionType userConnectionType)
        {
            UserId = userId;
            ConnectedUserId = connectedUserId;
            UserConnectionType = userConnectionType;
        }

        public int UserId { get; private set; }

        public int ConnectedUserId { get; private set; }

        public UserConnectionType UserConnectionType { get; private set; }
    }
}
