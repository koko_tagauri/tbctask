﻿using TbcTask.Domain.UserManagement.Enums;
using TbcTask.Shared;

namespace TbcTask.Domain.UserManagement.Models
{
    public class UserPhone : BaseEntity<int>
    {
        public UserPhone()
        {
        }

        public UserPhone(int userId, PhoneNumberType phoneNumberType, string phoneNumber)
        {
            UserId = userId;
            PhoneNumberType = phoneNumberType;
            PhoneNumber = phoneNumber;
        }

        public UserPhone(PhoneNumberType phoneNumberType, string phoneNumber)
        {
            PhoneNumberType = phoneNumberType;
            PhoneNumber = phoneNumber;
        }

        public int UserId { get; private set; }

        public virtual User User { get; private set; }

        public PhoneNumberType PhoneNumberType { get; private set; }

        public string PhoneNumber { get; private set; }
    }
}
