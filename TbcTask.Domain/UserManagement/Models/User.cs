﻿using System;
using System.Collections.Generic;
using TbcTask.Domain.CityManagement.Models;
using TbcTask.Domain.UserManagement.Enums;
using TbcTask.Shared;

namespace TbcTask.Domain.UserManagement.Models
{
    public class User : BaseEntity<int>
    {
        public User()
        {
        }

        public User(string firstName, string lastName, Gender gender, string pin, DateTime birthDate, int cityId, ICollection<UserPhone> userPhones)
        {
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
            Pin = pin;
            BirthDate = birthDate;
            CityId = cityId;

            UserPhones = userPhones ?? new List<UserPhone>();
        }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public Gender Gender { get; private set; }

        public string Pin { get; private set; }

        public DateTime BirthDate { get; private set; }

        public int CityId { get; private set; }

        public virtual City City { get; private set; }

        public string ImageUrl { get; private set; }

        public virtual ICollection<UserPhone> UserPhones { get; private set; }

        public void ChangeDetails(string firstName, string lastName, Gender gender, string pin, DateTime birthDate,
            int cityId, ICollection<UserPhone> userPhones)
        {
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
            Pin = pin;
            BirthDate = birthDate;
            CityId = cityId;

            UserPhones.Clear();
            UserPhones = userPhones;
        }

        public void ChangeImageUrl(string imageUrl)
        {
            ImageUrl = imageUrl;
        }
    }
}
