﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TbcTask.Domain.Interfaces;
using TbcTask.Domain.UserManagement.Models;

namespace TbcTask.Domain.UserManagement
{
    public interface IUserConnectionRepository : IRepository<UserConnection, int>
    {
        Task<UserConnection> GetByConnectionDetailsAsync(int userId, int connectedUserId);

        IEnumerable<UserConnection> GetUserConnections(int userId);

        void RemoveConnections(int userId);
    }
}
