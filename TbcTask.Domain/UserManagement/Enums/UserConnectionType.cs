﻿namespace TbcTask.Domain.UserManagement.Enums
{
    public enum UserConnectionType
    {
        Colleague = 1,
        Acquaintance = 2,
        Relative = 3,
        Other = 4
    }
}
