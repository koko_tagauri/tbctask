﻿namespace TbcTask.Domain.UserManagement.Enums
{
    public enum Gender
    {
        Male = 1,
        Female = 2
    }
}
