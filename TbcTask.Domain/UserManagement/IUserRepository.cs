﻿using TbcTask.Domain.Interfaces;
using TbcTask.Domain.UserManagement.Models;

namespace TbcTask.Domain.UserManagement
{
    public interface IUserRepository : IRepository<User, int>
    {
    }
}
