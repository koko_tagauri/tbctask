﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TbcTask.Application.Helpers;
using TbcTask.Application.Services.Users;
using TbcTask.Domain.Interfaces;
using TbcTask.Domain.UserManagement;
using TbcTask.Infrastructure;
using TbcTask.Infrastructure.DB;
using TbcTask.Infrastructure.Repositories;

namespace TbcTask.DI
{
    public class TbcTaskDependencyResolver
    {
        private IConfiguration Configuration { get; }

        public TbcTaskDependencyResolver(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IServiceCollection Resolve(IServiceCollection services)
        {
            services.AddDbContext<TbcTaskDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("TbcTaskDbContext")).UseLazyLoadingProxies());

            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<FileManager, FileManager>();

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserConnectionRepository, UserConnectionRepository>();
            services.AddScoped<IUsersService, UsersService>();

            return null;
        }
    }
}
