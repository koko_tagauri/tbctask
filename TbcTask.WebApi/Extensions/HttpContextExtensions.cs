﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace TbcTask.WebApi.Extensions
{
    public static class HttpContextExtensions
    {
        public static UnprocessableEntityObjectResult GetUnprocessableEntityObjectResult(this HttpContext httpContext, ValidationProblemDetails problemDetails)
        {
            FillValidationProblemDetails(httpContext, problemDetails);

            return new UnprocessableEntityObjectResult(problemDetails)
            {
                ContentTypes = { "application/problem+json" }
            };
        }

        private static void FillValidationProblemDetails(HttpContext httpContext, ValidationProblemDetails problemDetails)
        {
            problemDetails.Title = "One or more model validation errors occurred.";
            problemDetails.Status = StatusCodes.Status422UnprocessableEntity;
            problemDetails.Detail = "See the errors property for details.";
            problemDetails.Instance = httpContext.Request.Path;
            problemDetails.Extensions.Add("traceId", httpContext.TraceIdentifier);
        }
    }
}
