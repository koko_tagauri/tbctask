﻿using System.Linq;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;

namespace TbcTask.WebApi.Extensions
{
    public static class ValidationExceptionExtensions
    {
        public static ValidationProblemDetails GetValidationProblemDetails(this ValidationException validationException)
        {
            var errors = validationException.Errors.ToList();

            if (!validationException.Errors.Any())
            {
                errors.Add(new ValidationFailure("content", validationException.Message));
            }

            var errorsDictionary = errors.ToDictionary(error => error.PropertyName, error => new[] { error.ErrorMessage });

            var validationProblemDetails = new ValidationProblemDetails(errorsDictionary);

            return validationProblemDetails;
        }
    }
}
