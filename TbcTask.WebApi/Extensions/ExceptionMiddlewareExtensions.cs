﻿using Microsoft.AspNetCore.Builder;
using TbcTask.WebApi.Middlewares;

namespace TbcTask.WebApi.Extensions
{
    public static class ExceptionMiddlewareExtensions
    {
        public static IApplicationBuilder UseExceptionHandling(this IApplicationBuilder builder) =>
            builder.UseMiddleware<ExceptionMiddleware>();
    }
}
