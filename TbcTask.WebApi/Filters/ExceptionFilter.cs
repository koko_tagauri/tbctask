﻿using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using TbcTask.Domain.Exceptions;
using TbcTask.WebApi.Extensions;

namespace TbcTask.WebApi.Filters
{
    public class ExceptionFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
            switch (context.Exception)
            {
                case EntityNotFoundException _:
                    context.Result = new NotFoundResult();
                    context.ExceptionHandled = true;
                    break;
                 
                case ValidationException validationException:
                    context.Result = context.HttpContext.GetUnprocessableEntityObjectResult(validationException.GetValidationProblemDetails());
                    context.ExceptionHandled = true;
                    break;

                case null:
                    return;
            }
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
        }
    }
}
