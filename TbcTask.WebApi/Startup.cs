using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Linq;
using System.Reflection;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Localization;
using TbcTask.DI;
using TbcTask.WebApi.Extensions;
using TbcTask.WebApi.Filters;

namespace TbcTask.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo());

                c.CustomSchemaIds(sc => sc.FullName);
            });

            new TbcTaskDependencyResolver(Configuration).Resolve(services);

            services.AddCors();

            services.AddControllers(options => 
                    options.Filters.Add(typeof(ExceptionFilter)))
                .AddJsonOptions(options =>
                    {
                        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                    })
                .AddFluentValidation(options =>
                {
                    options.RegisterValidatorsFromAssembly(GetApplicationAssembly());
                    options.ImplicitlyValidateChildProperties = true;
                })
                .ConfigureApiBehaviorOptions(setupAction =>
                {
                    setupAction.InvalidModelStateResponseFactory = context =>
                    {
                        var errors = context.ModelState.ToDictionary(item => item.Key,
                            item => item.Value.Errors.Select(x => x.ErrorMessage).ToArray());

                        var result =
                            context.HttpContext.GetUnprocessableEntityObjectResult(
                                new ValidationProblemDetails(errors));

                        return result;
                    };
                });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseExceptionHandling();

            app.Use((context, next) =>
            {
                var userLanguages = context.Request.Headers["Accept-Language"].ToString();
                var firstLang = userLanguages.Split(',').FirstOrDefault();

                var lang = firstLang switch
                {
                    "ka" => firstLang,
                    "ru" => firstLang,
                    "en" => firstLang,
                    _ => "ka"
                };

                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(lang);
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

                context.Items["ClientLang"] = lang;
                context.Items["ClientCulture"] = Thread.CurrentThread.CurrentUICulture.Name;

                return next();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
            });

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        private static Assembly GetApplicationAssembly()
        {
            return AppDomain.CurrentDomain.GetAssemblies().Single(m => m.GetName().Name == "TbcTask.Application");
        }
    }
}
