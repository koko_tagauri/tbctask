﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TbcTask.Application.Services.Users;
using TbcTask.Application.Services.Users.Models;

namespace TbcTask.WebApi.Controllers.V1
{
    [Route("api/v1/users")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _usersService;

        public UsersController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(GetUsersResponse), StatusCodes.Status200OK)]
        public async Task<GetUsersResponse> Get([FromQuery] GetUsersRequest request) =>
           await _usersService.GetUsers(request);

        [HttpGet("connections-report")]
        [ProducesResponseType(typeof(GetUsersConnectionsResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public GetUsersConnectionsResponse GetConnectionsReport() =>
            _usersService.GetUserConnectionsReport();

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(GetUserDetailsResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<GetUserDetailsResponse> GetDetails(int id) =>
            await _usersService.GetUserDetails(id);

        [HttpPost]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<int> Create(CreateOrUpdateUserRequest request) =>
            await _usersService.CreateUser(request);

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<int> ChangeDetails(int id, CreateOrUpdateUserRequest request) =>
            await _usersService.ChangeUserDetails(id, request);

        [HttpPost("{id}/image")]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<int> UploadImage(int id, [FromForm] UploadUserImageRequest request) =>
            await _usersService.UploadUserImage(id, request);

        [HttpPatch("{id}/add-connected-user")]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<int> AddConnectedUser(int id, AddConnectedUserRequest request) =>
            await _usersService.AddConnectedUser(id, request);

        [HttpPatch("{id}/remove-connected-user")]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<int> RemoveConnectedUser(int id, RemoveConnectedUserRequest request) =>
            await _usersService.RemoveConnectedUser(id, request);

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<int> ChangeDetails(int id) =>
            await _usersService.DeleteUser(id);
    }
}
