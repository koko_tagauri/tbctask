﻿using System;

namespace TbcTask.Shared.Contracts
{
    public interface IChangeDateTrackedEntity
    {
        DateTimeOffset LastChangeDate { get; set; }
    }
}
