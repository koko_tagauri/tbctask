﻿using System;

namespace TbcTask.Shared.Contracts
{
    public interface ICreateDateTrackedEntity
    {
        DateTimeOffset CreateDate { get; set; }
    }
}
