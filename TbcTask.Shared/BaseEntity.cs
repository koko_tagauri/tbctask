﻿using System;
using TbcTask.Shared.Contracts;

namespace TbcTask.Shared
{
    public class BaseEntity<T> : ICreateDateTrackedEntity, IChangeDateTrackedEntity
    {
        public T Id { get; protected set; }

        public DateTimeOffset CreateDate { get; set; }
        
        public DateTimeOffset LastChangeDate { get; set; }
    }
}
