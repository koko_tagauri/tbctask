﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;
using TbcTask.Application.Constants;

namespace TbcTask.Application.Helpers
{
    public class FileManager
    {
        private readonly IWebHostEnvironment _env;

        public FileManager(IWebHostEnvironment env)
        {
            _env = env;
        }

        public async Task<string> UploadImageAsync(int userId, IFormFile image)
        {
            var baseUrl = Path.Combine(UserConstants.UserImageBaseUrl, userId.ToString());
            var basePath = Path.Combine(_env.WebRootPath, baseUrl);

            if (!Directory.Exists(basePath))
                Directory.CreateDirectory(basePath);
            else
            {
                var di = new DirectoryInfo(basePath);

                foreach (var file in di.GetFiles()) 
                    file.Delete();
            }

            var fullPath = Path.Combine(basePath, image.FileName);

            await using var fileStream = new FileStream(fullPath, FileMode.Create);
            
            await image.CopyToAsync(fileStream);

            var url = Path.Combine(baseUrl, image.FileName);

            return url;
        }

        public void DeleteImageAsync(int userId)
        {
            var baseUrl = Path.Combine(UserConstants.UserImageBaseUrl, userId.ToString());
            var basePath = Path.Combine(_env.WebRootPath, baseUrl);

            if (Directory.Exists(basePath))
                Directory.Delete(basePath, true);
        }
    }
}
