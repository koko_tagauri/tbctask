﻿using System.IO;

namespace TbcTask.Application.Constants
{
    public static class UserConstants
    {
        public static string UserImageBaseUrl => Path.Combine("files", "users");
    }
}
