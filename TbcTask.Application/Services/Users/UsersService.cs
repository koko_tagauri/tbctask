﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TbcTask.Application.Extensions;
using TbcTask.Application.Helpers;
using TbcTask.Application.Services.Users.Models;
using TbcTask.Domain.Exceptions;
using TbcTask.Domain.Interfaces;
using TbcTask.Domain.UserManagement.Models;

namespace TbcTask.Application.Services.Users
{
    public class UsersService : ServiceBase, IUsersService
    {
        public UsersService(IUnitOfWork unitOfWork, IServiceProvider serviceProvider) : base(unitOfWork, serviceProvider)
        {
        }

        public async Task<GetUsersResponse> GetUsers(GetUsersRequest model)
        {
            var totalCount = await Uow.UserRepository.CountAsync();

            var query = Uow.UserRepository.TableNoTracking
                .And(model.SearchTerm, x => x.FirstName.ToLower().Contains(model.SearchTerm.ToLower()) ||
                                            x.LastName.ToLower().Contains(model.SearchTerm.ToLower()) ||
                                            x.Pin.Contains(model.SearchTerm.ToLower()))
                .And(model.Gender, x => x.Gender == model.Gender)
                .And(model.BirthDate, x => x.BirthDate == model.BirthDate)
                .And(model.CityId, x => x.CityId == model.CityId);

            var filteredCount = await query.CountAsync();

            var items = query.TakePage(model.PageIndex, model.PageSize);

            var users = items.Select(x => new GetUserItem
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Gender = x.Gender,
                Pin = x.Pin,
                BirthDate = x.BirthDate,
                CityName = x.City.Name
            });

            var result = new GetUsersResponse
            {
                Items = users,
                TotalCount = totalCount,
                FilteredCount = filteredCount
            };

            return result;
        }

        public GetUsersConnectionsResponse GetUserConnectionsReport()
        {
            var users = Uow.UserRepository.TableNoTracking.ToList();

            var usersConnections = new List<GetUserConnectionsItem>();

            foreach (var user in users)
            {
                var connections = Uow.UserConnectionRepository.GetUserConnections(user.Id)
                    .GroupBy(x => x.UserConnectionType)
                    .Select(x => new
                    {
                        connectionType = x.Key,
                        connectionCount = x.Count()
                    });

                var connectionsByType = new List<GetUserConnectionByTypeItem>();

                connectionsByType.AddRange(connections.Select(userConnection => new GetUserConnectionByTypeItem
                {
                    UserConnectionType = userConnection.connectionType,
                    ConnectionsCount = userConnection.connectionCount
                }));

                usersConnections.Add(new GetUserConnectionsItem
                {
                    UserId = user.Id,
                    UserConnectionByTypeItems = connectionsByType
                });
            }

            return new GetUsersConnectionsResponse
            {
                UsersConnections = usersConnections
            };
        }

        public async Task<GetUserDetailsResponse> GetUserDetails(int userId)
        {
            var user = await Uow.UserRepository.GetByIdAsync(userId);

            if (user == null)
                throw new EntityNotFoundException();

            var connections = Uow.UserConnectionRepository.GetUserConnections(userId);

            var userDetailsResult = new GetUserDetailsResponse
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Gender = user.Gender,
                Pin = user.Pin,
                BirthDate = user.BirthDate,
                CityId = user.CityId,
                ImageUrl = user.ImageUrl,
                UserConnections = connections.Select(x => new UserConnectionItem
                {
                    ConnectedUserId = x.ConnectedUserId,
                    UserConnectionType = x.UserConnectionType
                }),
                UserPhones = user.UserPhones.Select(x => new UserPhoneItem
                {
                    PhoneNumber = x.PhoneNumber,
                    PhoneNumberType = x.PhoneNumberType
                })
            };

            return userDetailsResult;
        }

        public async Task<int> CreateUser(CreateOrUpdateUserRequest model)
        {
            var userPhones = model.UserPhoneItems.Select(x => x.ToDomainModel()).ToList();

            var user = new User(
                model.FirstName,
                model.LastName,
                model.Gender,
                model.Pin,
                model.BirthDate,
                model.CityId,
                userPhones
                );

            await Uow.UserRepository.InsertAsync(user);
            await Uow.CommitAsync();

            return user.Id;
        }

        public async Task<int> ChangeUserDetails(int userId, CreateOrUpdateUserRequest model)
        {
            var user = await Uow.UserRepository.GetByIdAsync(userId);

            if (user == null)
                throw new EntityNotFoundException();

            var userPhones = model.UserPhoneItems.Select(x => x.ToDomainModel()).ToList();

            user.ChangeDetails(
                model.FirstName,
                model.LastName,
                model.Gender,
                model.Pin,
                model.BirthDate,
                model.CityId,
                userPhones);

            await Uow.CommitAsync();

            return user.Id;
        }

        public async Task<int> UploadUserImage(int userId, UploadUserImageRequest model)
        {
            var user = await Uow.UserRepository.GetByIdAsync(userId);

            if (user == null)
                throw new EntityNotFoundException();

            var fileManager = GetService<FileManager>();
            
            var userImageUrl = await fileManager.UploadImageAsync(userId, model.Image);

            user.ChangeImageUrl(userImageUrl);

            await Uow.CommitAsync();

            return user.Id;
        }

        public async Task<int> AddConnectedUser(int userId, AddConnectedUserRequest model)
        {
            if (!await Uow.UserRepository.ExistsAsync(userId))
                throw new EntityNotFoundException();

            var userConnection = new UserConnection(userId, model.ConnectedUserId, model.UserConnectionType);

            await Uow.UserConnectionRepository.InsertAsync(userConnection);
            await Uow.CommitAsync();

            return userConnection.Id;
        }

        public async Task<int> RemoveConnectedUser(int userId, RemoveConnectedUserRequest model)
        {
            if (!await Uow.UserRepository.ExistsAsync(userId))
                throw new EntityNotFoundException();

            var userConnection =
                await Uow.UserConnectionRepository.GetByConnectionDetailsAsync(userId, model.ConnectedUserId);

            var id = userConnection.Id;

            Uow.UserConnectionRepository.Delete(userConnection);
            await Uow.CommitAsync();

            return id;
        }

        public async Task<int> DeleteUser(int userId)
        {
            var user = await Uow.UserRepository.GetByIdAsync(userId);

            if (user == null)
                throw new EntityNotFoundException();

            var fileManager = GetService<FileManager>();

            fileManager.DeleteImageAsync(userId);

            Uow.UserConnectionRepository.RemoveConnections(userId);
            Uow.UserRepository.Delete(user);

            await Uow.CommitAsync();

            return userId;
        }
    }
}
