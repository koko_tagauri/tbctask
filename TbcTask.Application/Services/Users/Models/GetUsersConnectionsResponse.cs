﻿using System.Collections.Generic;
using TbcTask.Domain.UserManagement.Enums;

namespace TbcTask.Application.Services.Users.Models
{
    public class GetUsersConnectionsResponse
    {
        public IEnumerable<GetUserConnectionsItem> UsersConnections { get; set; }
    }

    public class GetUserConnectionsItem
    {
        public int UserId { get; set; }

        public IEnumerable<GetUserConnectionByTypeItem> UserConnectionByTypeItems { get; set; }
    }

    public class GetUserConnectionByTypeItem
    {
        public int ConnectionsCount { get; set; }

        public UserConnectionType UserConnectionType { get; set; }
    }
}
