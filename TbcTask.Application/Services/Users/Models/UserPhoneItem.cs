﻿using TbcTask.Domain.UserManagement;
using TbcTask.Domain.UserManagement.Enums;
using TbcTask.Domain.UserManagement.Models;

namespace TbcTask.Application.Services.Users.Models
{
    public class UserPhoneItem
    {
        public PhoneNumberType PhoneNumberType { get; set; }

        public string PhoneNumber { get; set; }

        public UserPhone ToDomainModel()
        {
            return new UserPhone(PhoneNumberType, PhoneNumber);
        }
    }
}
