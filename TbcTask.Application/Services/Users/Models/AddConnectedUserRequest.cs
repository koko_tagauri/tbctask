﻿using TbcTask.Domain.UserManagement.Enums;

namespace TbcTask.Application.Services.Users.Models
{
    public class AddConnectedUserRequest
    {
        public int ConnectedUserId { get; set; }

        public UserConnectionType UserConnectionType { get; set; }
    }
}
