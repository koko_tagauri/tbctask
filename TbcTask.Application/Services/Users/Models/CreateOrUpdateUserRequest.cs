﻿using System;
using System.Collections.Generic;
using TbcTask.Domain.UserManagement.Enums;

namespace TbcTask.Application.Services.Users.Models
{
    public class CreateOrUpdateUserRequest
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Gender Gender { get; set; }

        public string Pin { get; set; }

        public DateTime BirthDate { get; set; }

        public int CityId { get; set; }

        public ICollection<UserPhoneItem> UserPhoneItems { get; set; }
    }
}
