﻿using Microsoft.AspNetCore.Http;

namespace TbcTask.Application.Services.Users.Models
{
    public class UploadUserImageRequest
    {
        public IFormFile Image { get; set; }
    }
}
