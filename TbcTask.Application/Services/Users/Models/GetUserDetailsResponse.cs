﻿using System;
using System.Collections.Generic;
using TbcTask.Domain.UserManagement.Enums;

namespace TbcTask.Application.Services.Users.Models
{
    public class GetUserDetailsResponse
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Gender Gender { get; set; }

        public string Pin { get; set; }

        public DateTime BirthDate { get; set; }

        public int CityId { get; set; }

        public string ImageUrl { get; set; }

        public IEnumerable<UserConnectionItem> UserConnections { get; set; }

        public IEnumerable<UserPhoneItem> UserPhones { get; set; }
    }

    public class UserConnectionItem
    {
        public int ConnectedUserId { get; set; }

        public UserConnectionType UserConnectionType { get; set; }
    }
}
