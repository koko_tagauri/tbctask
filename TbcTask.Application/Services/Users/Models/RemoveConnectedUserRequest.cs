﻿namespace TbcTask.Application.Services.Users.Models
{
    public class RemoveConnectedUserRequest
    {
        public int ConnectedUserId { get; set; }
    }
}
