﻿using System;
using TbcTask.Application.Infrastructure.PagingAndFiltering;
using TbcTask.Domain.UserManagement.Enums;

namespace TbcTask.Application.Services.Users.Models
{
    public class GetUsersResponse : PagedResponseModel<GetUserItem>
    {
    }

    public class GetUserItem
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Gender Gender { get; set; }

        public string Pin { get; set; }

        public DateTime BirthDate { get; set; }

        public string CityName { get; set; }
    }
}
