﻿using System;
using TbcTask.Application.Infrastructure.PagingAndFiltering;
using TbcTask.Domain.UserManagement.Enums;

namespace TbcTask.Application.Services.Users.Models
{
    public class GetUsersRequest : PagedRequest
    {
        public Gender? Gender { get; set; }

        public DateTime? BirthDate { get; set; }

        public int? CityId { get; set; }
    }
}
