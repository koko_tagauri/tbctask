﻿using System;
using FluentValidation;
using TbcTask.Application.Services.Users.Models;

namespace TbcTask.Application.Services.Users.Validators
{
    public class UserRequestValidator : AbstractValidator<CreateOrUpdateUserRequest>
    {
        public UserRequestValidator()
        {
            RuleFor(x => x.FirstName).Length(2, 50).WithMessage("Incorrect length")
                .Matches("^([\\u10D0-\\u10F0 ]+|[a-zA-Z ]+)$").WithMessage("Incorrect alphabetical format");
            RuleFor(x => x.LastName).Length(2, 50).WithMessage("Incorrect length")
                .Matches("^([\\u10D0-\\u10F0 ]+|[a-zA-Z ]+)$").WithMessage("Incorrect alphabetical format");
            RuleFor(x => x.Pin).Matches("^\\d{11}$").WithMessage("Incorrect Pin number format");
            RuleFor(x => x.BirthDate).LessThanOrEqualTo(DateTime.Now.AddYears(-18)).WithMessage("User must be minimum 18 years old");

            RuleForEach(x => x.UserPhoneItems).ChildRules(m =>
            {
                m.RuleFor(c => c.PhoneNumber).Length(4, 50).WithMessage("Incorrect length ");
            });
        }
    }
}