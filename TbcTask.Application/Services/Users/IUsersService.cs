﻿using System.Threading.Tasks;
using TbcTask.Application.Services.Users.Models;

namespace TbcTask.Application.Services.Users
{
    public interface IUsersService
    {
        Task<GetUsersResponse> GetUsers(GetUsersRequest model);

        GetUsersConnectionsResponse GetUserConnectionsReport();

        Task<GetUserDetailsResponse> GetUserDetails(int userId);

        Task<int> CreateUser(CreateOrUpdateUserRequest model);

        Task<int> ChangeUserDetails(int userId, CreateOrUpdateUserRequest model);

        Task<int> UploadUserImage(int userId, UploadUserImageRequest model);

        Task<int> AddConnectedUser(int userId, AddConnectedUserRequest model);

        Task<int> RemoveConnectedUser(int userId, RemoveConnectedUserRequest model);

        Task<int> DeleteUser(int userId);
    }
}
