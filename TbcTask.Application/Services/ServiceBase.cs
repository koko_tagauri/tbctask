﻿using System;
using TbcTask.Domain.Interfaces;

namespace TbcTask.Application.Services
{
    public abstract class ServiceBase
    {
        protected readonly IUnitOfWork Uow;
        protected IServiceProvider ServiceProvider;

        protected ServiceBase(IUnitOfWork unitOfWork, IServiceProvider serviceProvider)
        {
            Uow = unitOfWork;
            ServiceProvider = serviceProvider;
        }

        protected T GetService<T>()
        {
            return (T)ServiceProvider.GetService(typeof(T));
        }
    }
}
