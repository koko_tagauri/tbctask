﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace TbcTask.Application.Extensions
{
    public static class IQueryableExtensions
    {
        public static IQueryable<TSource> TakePage<TSource>(
            this IQueryable<TSource> source,
            int? pageIndex,
            int? pageSize
        ) where TSource : class
        {
            if (pageIndex.HasValue && pageSize.HasValue && pageIndex != 0)
                source = source.Skip(pageIndex.Value * pageSize.Value).Take(pageSize.Value);

            return source;
        }

        public static IQueryable<TSource> And<TSource, TFilter>(this IQueryable<TSource> source, TFilter? filter, Expression<Func<TSource, bool>> predicate)
            where TFilter : struct
        {
            if (predicate == null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            return filter.HasValue ? source.Where(predicate) : source;
        }

        public static IQueryable<TSource> And<TSource>(this IQueryable<TSource> source, string filter, Expression<Func<TSource, bool>> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            return !string.IsNullOrWhiteSpace(filter) ? source.Where(predicate) : source;
        }
    }
}
