﻿using System.Collections.Generic;

namespace TbcTask.Application.Infrastructure.PagingAndFiltering
{
    public class PagedResponseModel<T> where T : class
    {
        public IEnumerable<T> Items { get; set; }

        public int FilteredCount { get; set; }

        public int TotalCount { get; set; }
    }
}
