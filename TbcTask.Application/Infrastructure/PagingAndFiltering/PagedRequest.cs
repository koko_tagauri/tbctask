﻿namespace TbcTask.Application.Infrastructure.PagingAndFiltering
{
    public class PagedRequest
    {
        public int? PageIndex { get; set; }

        public int? PageSize { get; set; }

        public string SearchTerm { get; set; }
    }
}
